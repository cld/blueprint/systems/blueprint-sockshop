package services

/****************************************************************/
import (
	"context"
	"sync"
	"time"
	"github.com/google/uuid"
	"errors"
)

// TODO: As of now we lock the entire SessionStore when adding new entries, or modifying existing ones
// A more granular approach is needed.

//TODO: We assumed in the Python implementation that the logInCookie is not renewed since we don't check the expiry date.
// However we might want to implement this check, and consequently return a new loginCookie in any route that notices it has expired, or alternatively removes the session for that user (logs them out)

/************************************HELPERS & MIDDLEWARE *************************************/

type SessionData struct{
	customerId string
	startAt string // should be Date
}

type SessionStore interface{
	startNewSession() string
	isSessionActive(id string) bool
	endSession(id string)
	getSession(id string) *SessionData
	addCustomerIdToSession(id, value string) //so far only string values can be added
}

type SessionStoreImpl struct{
	innerStore map[string]*SessionData 
	mutexLocker sync.Mutex

}

func (ss* SessionStoreImpl) startNewSession() string{
	ss.mutexLocker.Lock()
	sessionKey := uuid.New().String()
	ss.innerStore[sessionKey] = &SessionData{
		startAt: time.Now().Format("2006-01-02 15:04:05"),
	}
	ss.mutexLocker.Unlock()

	return sessionKey
}

func (ss* SessionStoreImpl) isSessionActive(id string) bool{
	s := ss.getSession(id)
	return s != nil
}

func (ss* SessionStoreImpl) endSession(id string) {
	ss.mutexLocker.Lock()
	delete (ss.innerStore, id)
	ss.mutexLocker.Unlock()
}

func (ss* SessionStoreImpl) getSession(id string) *SessionData{

	if session, ok := ss.innerStore[id]; ok {
		return session
	}
	return nil
}


func (ss* SessionStoreImpl) addCustomerIdToSession(id, value string){

	session := ss.getSession(id)
	ss.mutexLocker.Lock()
	//reassign
	ss.innerStore[id] = &SessionData{
		startAt: session.startAt,
		customerId: value,
	}
	ss.mutexLocker.Unlock()
}

/**********************************************************************************************/
func CreateFrontendService(userService UserService, cartService CartService, orderService OrderService, catalogueService CatalogueService) *FrontendImpl{ 

	return &FrontendImpl{
		userService,
		cartService,
		orderService,
		catalogueService,
		&SessionStoreImpl{},
	}
}

type Frontend interface{
	LoadPage(ctx context.Context, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	GetCards(ctx context.Context, cardId string, logInCookie, sessionCookie map[string]string) ([]Card, Cookies, error)
	PostCard(ctx context.Context, Card, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	DeleteCard(ctx context.Context, cardId string, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	GetCardsForCustomer(ctx context.Context, logInCookie, sessionCookie map[string]string) ([]Card, Cookies, error)
	GetCustomers(ctx context.Context, logInCookie, sessionCookie map[string]string) ([]Customer, Cookies, error)
	DeleteCustomer(ctx context.Context, customerId string, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	GetAddressesForCustomer(ctx context.Context, customerId string, logInCookie, sessionCookie map[string]string) ([]Address, Cookies, error)
	GetAddresses(ctx context.Context, sessionCookie map[string]string) ([]Address, Cookies, error)
	DeleteAddress(ctx context.Context, addressId string, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	PostAddress(ctx context.Context, address Address, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	PostCustomer(ctx context.Context, customer Customer, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	GetCustomer(ctx context.Context, logInCookie, sessionCookie map[string]string) (Customer, Cookies, error)
	Login(ctx context.Context, username, password string, sessionCookie map[string]string) (Customer, Cookies, error)
	Register(ctx context.Context, customer Customer, cards []Card, addresses []Address, sessionCookie map[string]string) (string, Cookies, error)
	GetCart(ctx context.Context, logInCookie, sessionCookie map[string]string) (Cart, []Item, Cookies, error)
	DeleteCart(ctx context.Context, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	DeleteItemFromCart(ctx context.Context, itemId string, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	AddItemToCart(ctx context.Context, itemId string, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	UpdateCartItem(ctx context.Context, itemId string, newQuantity uint16, logInCookie, sessionCookie map[string]string) (string, Cookies, error)
	GetTags(ctx context.Context, sessionCookie map[string]string) ([]string, Cookies, error)
	GetCatalogueSize(ctx context.Context, tags []string, sessionCookie map[string]string) (uint32, Cookies, error)
	GetCatalogue(ctx context.Context, tags []string, orderId string, pageNum, pageSize uint16, sessionCookie map[string]string) ([]AlternativeSock, Cookies, error)
	GetCatalogueItem(ctx context.Context, itemId string, sessionCookie map[string]string) (AlternativeSock, Cookies, error)
	GetOrders(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) ([]Order, Cookies, error)
	GetOrder(ctx context.Context, orderId string, logInCookie map[string]string, sessionCookie map[string]string) (Order, Cookies, error)
	PostOrder(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error)
}


type FrontendImpl struct {
	userService UserService
	cartService CartService
	orderService OrderService
	catalogueService CatalogueService
	sessionStore SessionStore 
}

func (fi* FrontendImpl) getCustomerId(logInSessionId, activeSessionId string) string {
	if logInSessionId == ""{
		return activeSessionId
	}
	return fi.sessionStore.getSession(logInSessionId).customerId
}

func (fi* FrontendImpl) checkActiveSession(activeSessionId string) string {
	if activeSessionId != "" && fi.sessionStore.isSessionActive(activeSessionId){
		return activeSessionId
	}
	return fi.sessionStore.startNewSession()
}

func (fi* FrontendImpl) parseCookie(cookie map[string]string) string{ //this getfs the ID field in each cookie
	if cookie == nil{
		return ""
	}
	return cookie["id"]
}

func (fi* FrontendImpl) LoadPage(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error) {
	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	if logInCookie == nil{

		return customerId, Cookies{
			SessionCookie: map[string]string{
				"id": sessionId,
				"expires_at": "never",
			}}, nil
	}

	return customerId, Cookies{
			SessionCookie: sessionCookie,
			LogInCookie: logInCookie,
		}, nil
}

func (fi* FrontendImpl) GetCards(ctx context.Context, cardId string, logInCookie map[string]string, sessionCookie map[string]string) ([]Card, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	var cards []Card
	
	if cardId != "" {
		card, err := fi.userService.GetCard(ctx, cardId)
		if err != nil {
			return nil, Cookies{}, err
		}
		cards = append(cards, card)
	} else{
		var err error
		cards, err = fi.userService.GetCards(ctx)
		if err != nil {
			return nil, Cookies{}, err
		}
	}
	
	return cards,
		Cookies{
			SessionCookie: map[string]string{
				"id": sessionId,
				"expires_at": "never",
		},
	}, nil
}

func (fi* FrontendImpl) PostCard(ctx context.Context, card Card, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error){

	if card.LongNum == "" || card.Cvv == "" || card.Expires == ""{
		return "", Cookies{}, errors.New("Invalid card details")
	}

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	cardId, err := fi.userService.AddCard(ctx, card)
	
	if err != nil {
		return "",Cookies{}, err
	}

	return cardId, Cookies{
			SessionCookie: map[string]string{
				"id": sessionId,
				"expires_at": "never",	
			},
		}, nil
}

func (fi* FrontendImpl) DeleteCard(ctx context.Context, cardId string, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error){

	if cardId == ""{
		return "",Cookies{}, errors.New("No card ID provided!")
	}

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	_, err := fi.userService.DeleteCard(ctx, cardId)
	if err != nil {
		return "", Cookies{}, err
	}

	return "Card removed successfully", Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCardsForCustomer(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) ([]Card, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	cards, err := fi.userService.GetCardsForCustomer(ctx, customerId)
	if err != nil {
		return nil, Cookies{}, err
	}
	return cards, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCustomers(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) ([]Customer, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	customers, err := fi.userService.GetCustomers(ctx)
	if err != nil {
		return nil, Cookies{}, err
	}

	return customers, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) DeleteCustomer(ctx context.Context, customerId string, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	msg, err := fi.userService.DeleteCustomer(ctx, customerId)
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetAddressesForCustomer(ctx context.Context, customerId string, logInCookie map[string]string, sessionCookie map[string]string) ([]Address, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	addrs, err :=  fi.userService.GetAddressesForCustomer(ctx, customerId)
	if err != nil {
		return nil, Cookies{}, err
	}

	return addrs, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

/***************************************************************************************/

func (fi* FrontendImpl) GetAddresses(ctx context.Context, sessionCookie map[string]string) ([]Address, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	addrs, err := fi.userService.GetAddresses(ctx)
	if err != nil {
		return nil, Cookies{}, err
	}

	return addrs, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil

}

func (fi* FrontendImpl) DeleteAddress(ctx context.Context, addressId string, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){

	if addressId == ""{
		return "", Cookies{}, errors.New("No address ID provided")
	}

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	msg, err :=  fi.userService.DeleteAddress(ctx, addressId, customerId)
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) PostAddress(ctx context.Context, address Address, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	addrId, err := fi.userService.AddAddress(ctx, customerId, address)
	if err != nil {
		return "", Cookies{}, err
	}

	return addrId, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) PostCustomer(ctx context.Context, customer Customer, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	customerId, err := fi.userService.AddCustomer(ctx, customer, []Card{}, []Address{})
	if err != nil {
		return "", Cookies{}, err
	}

	return customerId, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCustomer(ctx context.Context, logInCookie map[string]string,sessionCookie map[string]string) (Customer, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	if customerId == sessionId{
		return Customer{}, Cookies{}, errors.New("Please log in first")
	}

	customer, err := fi.userService.GetCustomerById(ctx, customerId)
	if err != nil {
		return Customer{}, Cookies{}, err
	}

	return customer, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}


func (fi* FrontendImpl) Login(ctx context.Context, username string, password string, sessionCookie map[string]string) (Customer, Cookies, error){
	
	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	customer, err := fi.userService.Login(ctx, username, password)
	if err != nil {
		return Customer{}, Cookies{}, err
	}

	_, err = fi.cartService.MergeCarts(ctx, customer.Id, sessionId)
	if err != nil {
		return Customer{}, Cookies{}, err
	}

	return customer, Cookies{
		LogInCookie: map[string]string{
			"id": sessionId,
			"expires_at":"never",
		},
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at":"never",
		},
	}, nil

}

func (fi* FrontendImpl) Register(ctx context.Context, customer Customer, cards []Card, addresses []Address, sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	customerId, err := fi.userService.AddCustomer(ctx, customer, cards, addresses)
	if err != nil {
		return "", Cookies{}, err
	}

	_, err = fi.cartService.MergeCarts(ctx, customerId, sessionId)
	if err != nil {
		return "", Cookies{}, err
	}

	return customerId, Cookies{
		LogInCookie: map[string]string{
			"id": sessionId,
			"expires_at":"never",
		},
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at":"never",
		},
	}, nil
}

/********************************************************************************/

func (fi* FrontendImpl) GetCart(ctx context.Context, logInCookie map[string]string,sessionCookie map[string]string) (Cart, []Item,  Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	cart, items, err := fi.cartService.GetCartItems(ctx, customerId)
	if err != nil {
		return Cart{},nil,  Cookies{}, err
	}

	return cart, items, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) DeleteCart(ctx context.Context, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	msg, err := fi.cartService.DeleteCart(ctx, customerId)
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) DeleteItemFromCart(ctx context.Context, itemId string, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	msg, err := fi.cartService.RemoveItemFromCart(ctx, customerId, itemId)
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) AddItemToCart(ctx context.Context, itemId string, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error) {
	if itemId == ""{
		return "", Cookies{}, errors.New("No item ID was provided")
	}

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	sock, err :=  fi.catalogueService.GetSock(ctx, itemId)
	if err != nil {
		return "", Cookies{}, err
	}

	msg, err := fi.cartService.AddItemToCart(ctx, customerId, sock.Id, sock.Price) // need to cast before trying .id
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",
		},
	}, nil
}

func (fi* FrontendImpl) UpdateCartItem(ctx context.Context, itemId string, newQuantity uint16, logInCookie map[string]string,sessionCookie map[string]string) (string, Cookies, error){
	if itemId == ""{
		return "", Cookies{}, errors.New("No item ID was provided")
	}

	if newQuantity < 1{
		return "", Cookies{}, errors.New("Invalid quantity provided")
	}

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	sock, err := fi.catalogueService.GetSock(ctx, itemId)
	if err != nil {
		return "", Cookies{}, err
	}

	msg, err := fi.cartService.UpdateItemInCart(ctx, customerId, sock.Id, sock.Price, newQuantity) // need to cast before trying .id
	if err != nil {
		return "", Cookies{}, err
	}

	return msg, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

/*************************************************************************************/

func (fi* FrontendImpl) GetTags(ctx context.Context, sessionCookie map[string]string) ([]string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	tags, err := fi.catalogueService.GetTags(ctx)
	if err != nil {
		return nil, Cookies{}, err
	}

	return tags, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCatalogueSize(ctx context.Context, tags []string, sessionCookie map[string]string) (uint32, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	count, err := fi.catalogueService.GetCatalogueSize(ctx, tags)

	if err != nil {
		return 0, Cookies{}, err
	}

	return count, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCatalogue(ctx context.Context, tags []string, orderId string, pageNum uint16, pageSize uint16, sessionCookie map[string]string) ([]AlternativeSock, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	socks, err := fi.catalogueService.GetSocks(ctx, tags, orderId, pageNum, pageSize)
	if err != nil {
		return nil, Cookies{}, err
	}

	return socks, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetCatalogueItem(ctx context.Context, itemId string, sessionCookie map[string]string) (AlternativeSock, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	sock, err := fi.catalogueService.GetSock(ctx, itemId)
	if err != nil {
		return AlternativeSock{}, Cookies{}, err
	}

	return sock, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

/*************************************************************************************/

func (fi* FrontendImpl) GetOrders(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) ([]Order, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	if customerId == sessionId{
		return nil, Cookies{}, errors.New("User is not logged in")
	}

	orders, err := fi.orderService.GetOrders(ctx, customerId, "date") // sortBy = "date"
	if err != nil {
		return nil, Cookies{}, err
	}

	return orders, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) GetOrder(ctx context.Context, orderId string, logInCookie map[string]string, sessionCookie map[string]string) (Order, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))

	order, err := fi.orderService.GetOrder(ctx, orderId)
	if err != nil {
		return Order{}, Cookies{}, err
	}

	return order, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}

func (fi* FrontendImpl) PostOrder(ctx context.Context, logInCookie map[string]string, sessionCookie map[string]string) (string, Cookies, error){

	sessionId := fi.checkActiveSession(fi.parseCookie(sessionCookie))
	customerId := fi.getCustomerId(fi.parseCookie(logInCookie), sessionId)

	if customerId == sessionId{
		return "", Cookies{}, errors.New("User is not logged in!")
	}

	customer, err := fi.userService.GetCustomerById(ctx, customerId)
	if err != nil {
		return "", Cookies{}, err
	}

	address_link := customer.Addresses[0].Link["Href"]
	card_link := customer.Cards[0].Link["Href"]
	items_link := "cartService.getCartItems/" + customerId

	
	var wg sync.WaitGroup
	var err1, err2 error
	go func() { 
		defer wg.Done()
		_, err1 = fi.userService.GetCardsForCustomer(ctx, customerId)
	}()	
	go func() { 
		defer wg.Done()
		_, err2 = fi.userService.GetAddressesForCustomer(ctx, customerId)
	}()	
	wg.Wait()
	
	if err1 != nil {
		return "", Cookies{}, err1
	}

	if err2 != nil {
		return "", Cookies{}, err2
	}

	orderId, err := fi.orderService.PostOrder(ctx, map[string]string{
		"customer": customer.Id,
		"address": address_link,
		"card": card_link,
		"items": items_link,
	})

	return orderId, Cookies{
		SessionCookie: map[string]string{
			"id": sessionId,
			"expires_at": "never",	
		},
	}, nil
}
