package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"errors"
	"strings"
	"fmt"
	"github.com/google/uuid"
	"sync"
)

func CreateOrderService(db components.NoSQLDatabase, userService UserService, cartService CartService, paymentService PaymentService, shippingService ShippingService) *OrderServiceImpl { 

	return &OrderServiceImpl{
		userService: userService,
		cartService: cartService,
		paymentService: paymentService,
		shippingService: shippingService,
		db: db,
	}
}
type OrderService interface{
	GetOrder(ctx context.Context, orderId string) (Order, error)
	GetOrders(ctx context.Context, customerId, sortBy string) ([]Order, error)
	PostOrder(ctx context.Context, orderLinks map[string]string) (string, error)
}

type OrderServiceImpl struct{
	db components.NoSQLDatabase
	userService UserService
	cartService CartService
	paymentService PaymentService
	shippingService ShippingService
}


func (os *OrderServiceImpl) GetOrder(ctx context.Context, orderId string) (Order, error){

	ordersCollection := os.db.GetDatabase("data").GetCollection("customerOrders")

	query := fmt.Sprintf(`{"Id": %s }`, orderId)

	result, err := ordersCollection.FindOne(query)

	if err != nil{
		return Order{}, err
	}

	var order Order
	err = result.Decode(&order)

	return order, nil
}	
func (os *OrderServiceImpl) GetOrders(ctx context.Context, customerId, sortBy string) ([]Order, error){
	
	ordersCollection := os.db.GetDatabase("data").GetCollection("customerOrders")

	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, err := ordersCollection.FindMany(query)

	if err != nil{
		return nil, err
	}

	var orders []Order
	err = result.All(&orders)

	return orders, nil
}

func (os *OrderServiceImpl) parseLink(resourceUri string) string{

	return strings.Split(resourceUri, "/")[2]
}

func (os *OrderServiceImpl) calculateTotal(items []Item) float32{


	shippingFee := float32(4.99)
	var quantity, unitPrice, total float32
	for _, v := range items{
		
		quantity = float32(v.Quantity)
		unitPrice = float32(v.UnitPrice)
		total += quantity * unitPrice
	}

	return total + shippingFee
}

func (os *OrderServiceImpl) PostOrder(ctx context.Context, orderLinks map[string]string) (string, error){

	customerId := os.parseLink(orderLinks["customer"])
	addressCustomerId := os.parseLink(orderLinks["address"])
	cardsCustomerId := os.parseLink(orderLinks["card"])
	itemsCustomerId := os.parseLink(orderLinks["items"])
	
	if customerId != addressCustomerId || customerId != cardsCustomerId || customerId != itemsCustomerId {
		return "", errors.New("Inconsistent order details")
	}

	var wg sync.WaitGroup
	wg.Add(3)
	var cards []Card
	var addresses []Address
	var items []Item
	var err1, err2, err3 error

	go func() {
		defer wg.Done()
		cards, err1 = os.userService.GetCardsForCustomer(ctx, customerId)
	}()
	go func() {
		defer wg.Done()
		addresses, err2 = os.userService.GetAddressesForCustomer(ctx, customerId)
	}()
	go func() {
		defer wg.Done()
		_, items, err3 = os.cartService.GetCartItems(ctx, customerId)
	}()
	wg.Wait()

	if err1 != nil{
		return "", err1
	}
	if err2 != nil{
		return "", err2
	}
	if err3 != nil{
		return "", err3
	}

	amount := os.calculateTotal(items)

	authGranted, err := os.paymentService.Authorize(ctx, amount)
	if err != nil{
		return "", nil
	}

	if !authGranted{
		return "", errors.New("Could not authorize payment")
	}

	shipment := map[string]string{
		"name": customerId,
	}

	os.shippingService.PostShipping(ctx, shipment)

	order := Order{
		Id: uuid.New().String(), 
		CustomerId: customerId,
		Card: cards[0],
		Address: addresses[0],
		Items: items,
		Total: amount,
		Shipment: shipment,
	}

	ordersCollection := os.db.GetDatabase("data").GetCollection("customerOrders")

	err = ordersCollection.InsertOne(order)
	if err!= nil {
		return "", err
	}

	return order.Id, nil
}
