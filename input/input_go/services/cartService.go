package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"errors"
	"fmt"
	"github.com/google/uuid"
)

func CreateCartService(db components.NoSQLDatabase) *CartServiceImpl { 

	return &CartServiceImpl{
		db: db,
	}
}

type CartService interface{
	GetCartItems(ctx context.Context, customerId string, itemIds ...string) (Cart, []Item, error)
	DeleteCart(ctx context.Context, customerId string) (string, error)
	AddItemToCart(ctx context.Context, customerId, itemId string, unitPrice float32) (string, error)
	RemoveItemFromCart(ctx context.Context, customerId, itemId string) (string, error)
	UpdateItemInCart(ctx context.Context, customerId, itemId string, unitPrice float32, newQuantity uint16) (string, error)
	MergeCarts(ctx context.Context, customerId, sessionId string) (string, error)
}

type CartServiceImpl struct{
	db components.NoSQLDatabase
}

func (csi* CartServiceImpl) GetCartItems(ctx context.Context, customerId string, itemIds ...string) (Cart,[]Item, error){
	cartsCollection := csi.db.GetDatabase("data").GetCollection("carts")

	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, err := cartsCollection.FindOne(query)

	if err != nil{
		return Cart{}, nil, err
	}

	var cart Cart
	err = result.Decode(&cart)

	if err != nil {
		return Cart{}, nil, err
	}

	//! if cart.Id == "" { // this check should be performed inside the choice implementation of Decode; if the `cart` is not populated, return an error
	//!    . . .
	//! }

	newCart := Cart{
		Id: uuid.New().String(),
		CustomerId: customerId,
	}
	err = cartsCollection.InsertOne(newCart)

	if err != nil {
		return Cart{}, nil, err
	}

	return newCart, nil,  nil

	itemsCollection := csi.db.GetDatabase("data").GetCollection("items")

	var filter string
	if len(itemIds) > 0{

		filter = fmt.Sprintf(`{"ItemId": %s }`, itemIds[0])
	}else{
		
		filter = fmt.Sprintf(`{"CartId": %s }`, cart.Id)	
	}

	result, err = itemsCollection.FindMany(filter)

	if err!= nil{
		return Cart{}, nil, err
	}
	var items []Item

	result.All(&items)

	return newCart, items, nil
 }

func (csi* CartServiceImpl) DeleteCart(ctx context.Context, customerId string) (string, error){
	collection := csi.db.GetDatabase("data").GetCollection("carts")

	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	err := collection.DeleteOne(query) 

	if err != nil{
		return "", err
	}

	return "Cart removed successfully", nil
}

func (csi* CartServiceImpl) AddItemToCart(ctx context.Context, customerId, itemId string, unitPrice float32) (string, error){
	
	cartsCollection := csi.db.GetDatabase("data").GetCollection("carts")
	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, err := cartsCollection.FindOne(query)

	if err != nil{
		return "", err
	}

	var cart Cart
	err = result.Decode(&cart)

	if err != nil{
		return "", err
	}

	if cart.Id == ""{
		return "", errors.New("Cart does not exist")
	}

	
	foundInCart := false

	if cart.Items != nil && len(cart.Items) != 0 {
		for _, v := range cart.Items{
			if v == itemId{
				foundInCart = true
			}
		}
	}

	itemsCollection := csi.db.GetDatabase("data").GetCollection("items")
	
	if foundInCart {
		
		query = fmt.Sprintf(`{"ItemId": %s}`, itemId)
		projection := fmt.Sprintf(`{"$inc": {"Quantity": %d}`, 1)

		err = itemsCollection.UpdateOne(query, projection)

		if err != nil {
			return "", err
		}
		
	} else {
		
		item := Item{
			Id: itemId,
			Quantity: 1,
			UnitPrice: unitPrice,
			
		}
		err = itemsCollection.InsertOne(item)

		if err != nil {
			return "", err
		}
	}

	return  "Cart updated!", nil
}

func (csi* CartServiceImpl) RemoveItemFromCart(ctx context.Context, customerId, itemId string) (string, error){

	cartsCollection := csi.db.GetDatabase("data").GetCollection("carts")

	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, err := cartsCollection.FindOne(query)

	if err != nil{
		return "", err
	}

	var cart Cart
	err = result.Decode(&cart)

	if err != nil{
		return "", err
	}

	if cart.Id == ""{
		return "", errors.New("Cart does not exist")
	}

	
	foundInCart := false
	
	if cart.Items != nil && len(cart.Items) != 0 {
		for _, v := range cart.Items{
			if v == itemId{
				foundInCart = true
			}
		}
	}

	if !foundInCart{
		return "", errors.New("Item is not in cart")
	}

	itemsCollection := csi.db.GetDatabase("data").GetCollection("items")

	query = fmt.Sprintf(`{"ItemId": %s }`, itemId)

	result, err = itemsCollection.FindOne(query)

	if err != nil{
		return "", err
	}

	var item Item
	err = result.Decode(&item)

	if err != nil{
		return "", err
	}

	if item.Quantity > 1{

		projection := fmt.Sprintf(`{"$dec": {"Quantity": %d}`, 1)

		err = itemsCollection.UpdateOne(query, projection)

		if err != nil {
			return "", err
		}

	}else{ //quantity == 1


		err = itemsCollection.DeleteOne(query)

		if err != nil {
			return "", err
		}

		projection := fmt.Sprintf(`{"$pull": {"Items": {"$eq": %s}}`, itemId)

		err = cartsCollection.UpdateOne(query, projection)

		if err != nil {
			return "", err
		}
	}

	return "Cart updated!", nil
}

func (csi* CartServiceImpl) UpdateItemInCart(ctx context.Context, customerId, itemId string, unitPrice float32, newQuantity uint16) (string, error){
	cartsCollection := csi.db.GetDatabase("data").GetCollection("carts")

	query := fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, err := cartsCollection.FindOne(query)

	if err != nil{
		return "", err
	}
	var cart Cart
	err = result.Decode(&cart)

	if err != nil{
		return "", err
	}

	if cart.Id == ""{
		return "", errors.New("Cart does not exist")
	}

	foundInCart := false
	
	if cart.Items != nil && len(cart.Items) != 0 {
		for _, v := range cart.Items{
			if v == itemId{
				foundInCart = true
			}
		}
	}

	if !foundInCart{
		return "", errors.New("Item is not in cart")
	}

	itemsCollection := csi.db.GetDatabase("data").GetCollection("items")

	query = fmt.Sprintf(`{"ItemId": %s}`, itemId)
	projection := fmt.Sprintf(`{"$set": {"UnitPrice": %d, "Quantity: %d}}`, unitPrice, newQuantity)

	err = itemsCollection.UpdateOne(query, projection)
	if err != nil{
		return "", err
	}

	return "Cart updated!", nil
}

func (csi* CartServiceImpl) MergeCarts(ctx context.Context, customerId, sessionId string) (string, error){

	cartsCollection := csi.db.GetDatabase("data").GetCollection("carts")

	query := fmt.Sprintf(`{"CustomerId": %s }`, sessionId)

	result, _ := cartsCollection.FindOne(query)

	var sessionedCart, loggedInCart Cart
	result.Decode(&sessionedCart)

	if sessionedCart.Id == ""{
		return "", errors.New("Could not merge carts")
	}

	query = fmt.Sprintf(`{"CustomerId": %s }`, customerId)

	result, _ = cartsCollection.FindOne(query)

	result.Decode(&loggedInCart)

	var projection string 

	if loggedInCart.Id != ""{
		
		loggedInCart.Items = append(loggedInCart.Items, sessionedCart.Items...)

		query = fmt.Sprintf(`{"CustomerId": %s}`, customerId)
		projection = fmt.Sprintf(`{"$set": {Items: %v}}`, loggedInCart.Items)
	
		err := cartsCollection.UpdateOne(query, projection)

		if err != nil{
			return "", err
		}
	}else{

		cart := Cart{
			CustomerId: customerId,
			Items: sessionedCart.Items,
		}
		err := cartsCollection.InsertOne(cart) 
		if err != nil{
			return "", err
		}
	}

	query = fmt.Sprintf(`{"CustomerId": %s}`, customerId)

	err := cartsCollection.DeleteOne(query) 

	if err != nil{
		return "", err
	}

	return "Cart merged!", nil
}
