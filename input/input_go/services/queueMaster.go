package services

import (
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
)

func CreateQueueMaster(queue components.Queue) *QueueMasterImpl { 

	return &QueueMasterImpl{
		queue: queue,
	}
}

type QueueMaster interface{
	Entry() error
}

type QueueMasterImpl struct{
	queue components.Queue
}

// How is this used?
func(p* QueueMasterImpl) Entry() error {

	p.queue.Recv(func(data []byte){
		fmt.Printf("Got payload \n %s", data)
	})

	return nil
}