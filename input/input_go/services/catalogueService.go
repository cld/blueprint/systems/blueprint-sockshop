package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"errors"
	"strings"
	"io/ioutil"
)

func CreateCatalogueService(db components.RelationalDB) *CatalogueServiceImpl { 

	return &CatalogueServiceImpl{
		db: db,
		baseQuery: "SELECT sock.sock_id AS id, sock.name, sock.description, sock.price, sock.count, sock.image_url_1, sock.image_url_2, GROUP_CONCAT(tag.name) AS tag_name FROM sock JOIN sock_tag ON sock.sock_id=sock_tag.sock_id JOIN tag ON sock_tag.tag_id=tag.tag_id",
	}
}

type CatalogueService interface{
	GetSock(ctx context.Context, sockId string) (AlternativeSock, error)
	GetSocks(ctx context.Context, tags []string, orderId string, pageNum uint16, pageSize uint16) ([]AlternativeSock, error)
	GetCatalogueSize(ctx context.Context, tags []string) (uint32, error)
	GetTags(ctx context.Context) ([]string, error)
	InitializeSchema(ctx context.Context) error
}

type CatalogueServiceImpl struct{
	db components.RelationalDB
	baseQuery string
}

func(csi *CatalogueServiceImpl) InitializeSchema(ctx context.Context) error{

	query, err := ioutil.ReadFile("./schema.sql")
    if err != nil {
        return err
    }
	
	db, err := csi.db.Open("user", "pass", "data")
	if err != nil {
        return err
    }
	
    err = db.Exec(string(query))
    if err != nil {
        return err
    }
	return nil
}


func (csi *CatalogueServiceImpl) GetSock(ctx context.Context, sockId string) (AlternativeSock, error){

	if sockId == ""{
		return AlternativeSock{}, errors.New("No sock ID provided.")
	}

	query := csi.baseQuery + " WHERE sock.sock_id = ? GROUP BY sock.sock_id;"

	db, err := csi.db.Open("user", "pass", "data")

	if err != nil {
		return AlternativeSock{}, err
	}
	defer db.Close()

	qRes, _ := db.Query(query, sockId) //prepared statement

	var sock Sock

	if qRes.Next(){
		qRes.Scan(&sock.Id, &sock.Price, &sock.ImageUrl1, &sock.ImageUrl2)
	}else{
		return AlternativeSock{}, errors.New("Could not get sock")
	}

	return AlternativeSock{
		Id: sock.Id,
		Price: sock.Price,
		ImageUrl: []string{sock.ImageUrl1, sock.ImageUrl2},
		Tags:strings.Split(sock.TagName, ","),
	}, nil
}


func (csi *CatalogueServiceImpl)cut(socks []AlternativeSock, pageNum uint16, pageSize uint16) []AlternativeSock{

	if pageNum == 0 || pageSize == 0{
		return nil
	}

	start := (int(pageNum) * int(pageSize)) - int(pageSize)

	if start > len(socks){
		return nil
	}

	end := int(pageNum) * int(pageSize)

	if end > len(socks){
		end = len(socks)
	}

	return socks[start:end]
}

func (csi *CatalogueServiceImpl) GetSocks(ctx context.Context, tags []string, orderId string, pageNum uint16, pageSize uint16) ([]AlternativeSock, error){
	
	query := csi.baseQuery
	var args []interface{}
	for idx, tag := range tags{

		if idx == 0{
			query += " WHERE tag.name = ?"
		}else{
			query += " OR tag.name = ?"
		}
		args = append(args, tag)
	}

	query += " GROUP BY id"

	if orderId != ""{
		query += " ORDER BY ?"
		args = append(args, orderId)
	}

	query += ";"
	
	db, err := csi.db.Open("user", "pass", "data")
	if err != nil {
		return nil, err
	}
	defer db.Close()


	qRes, _ := db.Query(query, args...)

	var socks []AlternativeSock
	for qRes.Next(){
		var sock Sock
		qRes.Scan(&sock.Id, &sock.Price, &sock.ImageUrl1, &sock.ImageUrl2)

		socks = append(socks, AlternativeSock{
			Id: sock.Id,
			Price: sock.Price,
			ImageUrl: []string{sock.ImageUrl1, sock.ImageUrl2},
			Tags: strings.Split(sock.TagName, ","),
		})
	}

	return csi.cut(socks, pageNum, pageSize), nil
}

func (csi *CatalogueServiceImpl) GetCatalogueSize(ctx context.Context, tags []string) (uint32, error){
	query := "SELECT COUNT(DISTINCT sock.sock_id) FROM sock JOIN sock_tag ON sock.sock_id=sock_tag.sock_id JOIN tag ON sock_tag.tag_id=tag.tag_id"
	
	var args []interface{}
	for idx, tag := range tags{

		if idx == 0{
			query += " WHERE tag.name = ?"
		}else{
			query += " OR tag.name = ?"
		}
		args = append(args, tag[idx])
	}
	query += ";"


	db, err := csi.db.Open("user", "pass", "data")
	if err != nil {
		return 0, err
	}
	defer db.Close()

	qRes, _ := db.Query(query, args...)

	var count uint32

	if qRes.Next(){
		qRes.Scan(&count)
	}else{
		return 0, errors.New("Could not get catalogue size.")
	}

	return count, nil
}

func (csi *CatalogueServiceImpl) GetTags(ctx context.Context) ([]string, error){

	query := "SELECT name FROM tag;"

	db, err := csi.db.Open("user", "pass", "data")
	if err != nil {
		return nil, err
	}
	defer db.Close()

	qRes, _ := db.Query(query)

	var tags []string
	for qRes.Next(){
		var name string
		qRes.Scan(&name)
		tags = append(tags, name)
	} 
	
	if len(tags) == 0{
		return nil, errors.New("Could not find Tags.")
	}

	return tags, nil
}