// implement Remote for all relevant types

package services

type Cookies struct{
	SessionCookie map[string]string
	LogInCookie map[string]string
}
func (Cookies) remote(){}

//**************************

type AlternativeSock struct{
	Id string
	Price float32
	ImageUrl []string
	Tags []string
}
func (AlternativeSock) remote(){}
//**************************

type Sock struct {
	Id string
	Price float32
	ImageUrl1 string
	ImageUrl2 string
	TagName string
}

type Address struct{
	Id string
	Street string
	Number string
	Country string
	City string
	Postcode string
	Href string
	Link map[string]string
}

type Card struct{
	Id string 
	CustomerId string 
	LongNum string 
	Expires string 
	Cvv string
	Href string
	Link map[string]string
}

type Customer struct{
	Id string 
	Username string 
	Email string 
	Password string 
	FirstName string 
	LastName string 
	CardIds []string //* keeping only the IDs
	AddressIds []string 
	Addresses []Address //* keeping the entire thing
	Cards []Card
}

type Cart struct {
	Id string 
	CustomerId string 
	Items []string //* keeping only the IDs
}

type Item struct {
	Id string 
	Quantity uint16 
	UnitPrice float32
}

type Order struct {
	Id string 
	CustomerId string
	Card Card
	Address Address
	Items []Item 
	Total float32
	Shipment map[string]string
}

//TODO
//how to have these remote functions be more meaningful?
//what can the user implement so that their types implement the Remote interface?
//or can the Generator handle this somehow?


func (Sock) remote(){}
func (Address) remote(){}
func (Card) remote(){}
func (Customer) remote(){}
func (Cart) remote(){}
func (Item) remote(){}


