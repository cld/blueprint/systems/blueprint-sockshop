package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"encoding/json"
)

func CreateShippingService(queue components.Queue) *ShippingServiceImpl { 

	return &ShippingServiceImpl{
		queue: queue,
	}
}
type ShippingService interface{
	PostShipping(ctx context.Context, shipment map[string]string) (string, error)
}

type ShippingServiceImpl struct{
	queue components.Queue
}

func(p* ShippingServiceImpl) PostShipping(ctx context.Context, shipment map[string]string) (string, error){
	ship_bytes , err := json.Marshal(shipment)
	if err != nil {
		return "", err
	}
	p.queue.Send(ctx, ship_bytes)

	return "Shipment sent!", nil
}