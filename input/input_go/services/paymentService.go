package services

import (
	"context"
)

func CreatePaymentService() *PaymentServiceImpl { 

	return &PaymentServiceImpl{
		limitAmount: 2500.0,
	}
}
type PaymentService interface{
	Authorize(ctx context.Context, amount float32) (bool, error)
}

type PaymentServiceImpl struct{
	limitAmount float32
}

func(p* PaymentServiceImpl) Authorize(ctx context.Context, amount float32) (bool, error){

	if amount <= 0 || amount > p.limitAmount{
		return false, nil
	}

	return true, nil
}