module services

go 1.18

require (
	github.com/google/uuid v1.3.0
	gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler v0.0.1
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064
)

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/tracingplane/tracingplane-go v0.0.0-20171025152126-8c4e6f79b148 // indirect
	go.opentelemetry.io/otel v1.6.1 // indirect
	go.opentelemetry.io/otel/trace v1.6.1 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
