default_client_opts : Modifier = ClientPool(max_clients=10)
single_client_opts : Modifier = ClientPool(max_clients=1)
web_conn : Modifier = WebServer(framework="default")
http_conn : Modifier = WebServer(framework="default")

normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})
hidden_ports_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})

zipkinTracer : Tracer = ZipkinTracer().WithServer(normal_deployer)
zipkinTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=zipkinTracer, tracer_opts={service_name : x, sampling_rate: 1})

server_modifiers : Callable[str, List[Modifier]] = lambda x : [zipkinTracerModifier(x), http_conn, hidden_ports_deployer]
web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [zipkinTracerModifier(x), web_conn, normal_deployer]

userDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
userService : Service = UserServiceImpl(userDatabase = userDatabase).WithServer(web_server_modifiers("userService")).WithClient(default_client_opts)

cartDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
cartService : Service = CartServiceImpl(cartDatabase = cartDatabase).WithServer(web_server_modifiers("cartService")).WithClient(default_client_opts)

#payment
paymentService : Service = PaymentServiceImpl().WithServer(web_server_modifiers("paymentService")).WithClient(default_client_opts)
#shipping
queue_modifiers : Callable[str, List[Modifier]]  = lambda x : [zipkinTracerModifier(x), hidden_ports_deployer]
shippingQueue : Queue = RabbitMQ(queue_name="shipping_task").WithServer(queue_modifiers("shippingQueue"))
shippingService : Service = ShippingServiceImpl(shippingQueue = shippingQueue).WithServer(server_modifiers("shippingService")).WithClient(default_client_opts)
#orders
orderDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
orderService : Service = OrderServiceImpl(orderDatabase = orderDatabase, shippingService = shippingService, userService = userService, cartService = cartService, paymentService = paymentService).WithServer(web_server_modifiers("orderService")).WithClient(default_client_opts)

#queueMaster
consumer_queue_service_modifiers : Callable[str, List[Modifier]]  = lambda x : [zipkinTracerModifier(x), hidden_ports_deployer, web_conn]
queueMaster : Service = QueueMasterImpl(shippingQueue=shippingQueue).WithServer(consumer_queue_service_modifiers("queueMaster")).WithClient(default_client_opts)

#TODO add MySql instance to catalogueService
catalogueDatabase :  RelationalDB = MySqlDB().WithServer(hidden_ports_deployer)
catalogueService : Service = CatalogueServiceImpl(catalogueDatabase = catalogueDatabase).WithServer(web_server_modifiers("catalogueService")).WithClient(default_client_opts)

frontend : Service = FrontendImpl(userService = userService, cartService = cartService, catalogueService = catalogueService, orderService = orderService).WithServer(web_server_modifiers("frontend"))